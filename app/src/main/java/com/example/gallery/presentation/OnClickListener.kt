package com.example.gallery.presentation

import com.example.gallery.presentation.model.PresentationPhoto

interface OnClickListener {

    fun onClick(photo: PresentationPhoto)
}