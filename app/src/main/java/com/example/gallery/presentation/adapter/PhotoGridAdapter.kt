package com.example.gallery.presentation.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.gallery.databinding.GridViewItemBinding
import com.example.gallery.presentation.OnClickListener
import com.example.gallery.presentation.model.PresentationPhoto


class PhotoGridAdapter(
    private val onClickListener: OnClickListener
) : ListAdapter<PresentationPhoto, PhotoGridAdapter.ViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = GridViewItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position], onClickListener)
    }

    class ViewHolder(private val binding: GridViewItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(photo: PresentationPhoto, onClickListener: OnClickListener) = with(binding) {
            loadImage(photo, imageView)

            root.setOnClickListener {
                onClickListener.onClick(photo)
            }
        }

        private fun loadImage(photo: PresentationPhoto, imageView: ImageView) {
            Glide.with(imageView.context)
                .load(photo.url)
                .override(photo.listItemWidth, photo.listItemHeight)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        binding.progressCircular.visibility = View.GONE
                        return false
                    }
                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        binding.progressCircular.visibility = View.GONE
                        return false
                    }
                })
                .into(imageView)
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<PresentationPhoto>() {
        override fun areItemsTheSame(oldItem: PresentationPhoto, newItem: PresentationPhoto) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: PresentationPhoto, newItem: PresentationPhoto) =
            oldItem == newItem
    }
}