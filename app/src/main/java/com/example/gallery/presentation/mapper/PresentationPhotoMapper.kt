package com.example.gallery.presentation.mapper

import com.example.gallery.domain.model.Photo
import com.example.gallery.presentation.model.PresentationPhoto
import javax.inject.Inject
import kotlin.math.roundToInt

class PresentationPhotoMapper @Inject constructor(private val screenWidth: Int) {
    fun mapToPresentation(photo: Photo): PresentationPhoto {

        val adjustedWidth = (screenWidth / 2.0).roundToInt()
        val adjustedHeight = (1.0 * adjustedWidth * photo.height / photo.width).roundToInt()//

        return PresentationPhoto(
            id = photo.id,
            width = photo.width,
            height = photo.height,
            listItemWidth = adjustedWidth,
            listItemHeight = adjustedHeight,
            url = photo.srcUrl
        )
    }

    fun mapToPresentation(photoList: List<Photo>): List<PresentationPhoto> =
        photoList.map { mapToPresentation(it) }
}