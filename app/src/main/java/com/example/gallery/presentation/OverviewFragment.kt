package com.example.gallery.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.gallery.R
import com.example.gallery.databinding.FragmentOverviewBinding
import com.example.gallery.presentation.adapter.PhotoGridAdapter
import com.example.gallery.presentation.model.PresentationPhoto
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OverviewFragment : Fragment(), OnClickListener {

    private lateinit var binding: FragmentOverviewBinding
    private val viewModel: MainViewModel by viewModels(
        ownerProducer = { requireActivity() }
    )
    private lateinit var adapter: PhotoGridAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentOverviewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
    }

    private fun setUpView() {
        adapter = PhotoGridAdapter(this as OnClickListener)
        binding.photosGrid.adapter = adapter
        binding.refreshLayout.setOnRefreshListener {
            viewModel.getPhotos()
        }
        viewModel.photos.observe(viewLifecycleOwner) {
            when(it) {
                is MainViewModel.Event.Error -> {
                    showErrorMessage(it.message)
                    binding.refreshLayout.isRefreshing = false
                }
                is MainViewModel.Event.Success -> {
                    adapter.submitList(it.data)
                    binding.refreshLayout.isRefreshing = false
                }
            }
        }
    }

    override fun onClick(photo: PresentationPhoto) {
        val bundle = bundleOf(
            "width" to photo.width,
            "height" to photo.height,
            "url" to photo.url
        )
        findNavController().navigate(R.id.action_overviewFragment_to_imageDialog, bundle)
    }

    private fun showErrorMessage(message: String) =
        Snackbar.make(binding.root, message, Snackbar.LENGTH_SHORT).show()
}