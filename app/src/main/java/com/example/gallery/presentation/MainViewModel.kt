package com.example.gallery.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.gallery.domain.repository.PhotoRepository
import com.example.gallery.presentation.mapper.PresentationPhotoMapper
import com.example.gallery.presentation.model.PresentationPhoto
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: PhotoRepository,
    private val photoMapper: PresentationPhotoMapper
) : ViewModel() {

    private val _photos = MutableLiveData<Event>()
    val photos: LiveData<Event>
        get() = _photos

    sealed class Event {
        data class Success(val data: List<PresentationPhoto>) : Event()
        data class Error(val message: String) : Event()
    }

    init {
        getPhotos()
    }

    fun getPhotos() = viewModelScope.launch {
        val page = (0..10).random().toString()
        repository.getPhotos(page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { fetchedPhotos ->
                    _photos.value = Event.Success(photoMapper.mapToPresentation(fetchedPhotos))
                },
                {
                    it.printStackTrace()
                    _photos.value = Event.Error("Failed to load photos")
                }
            )
    }
}