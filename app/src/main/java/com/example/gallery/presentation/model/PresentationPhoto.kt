package com.example.gallery.presentation.model

data class PresentationPhoto(
    val id: String,
    val width: Int,
    val height: Int,
    val listItemWidth: Int,
    val listItemHeight: Int,
    val url: String
)