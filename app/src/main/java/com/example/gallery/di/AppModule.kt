package com.example.gallery.di

import android.app.Application
import com.example.gallery.data.PicsumApi
import com.example.gallery.data.mapper.DataPhotoMapper
import com.example.gallery.data.repository.PhotoRepositoryImpl
import com.example.gallery.domain.repository.PhotoRepository
import com.example.gallery.presentation.mapper.PresentationPhotoMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

private const val BASE_URL = "https://picsum.photos"

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun providePicsumApi(): PicsumApi {
        val loggingInterceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
            .create(PicsumApi::class.java)
    }

    @Provides
    @Singleton
    fun providePhotoRepository(api: PicsumApi, photoMapper: DataPhotoMapper): PhotoRepository =
        PhotoRepositoryImpl(api, photoMapper)

    @Provides
    @Singleton
    fun provideScreenWidth(app: Application): Int = app.resources.displayMetrics.widthPixels

    @Provides
    @Singleton
    fun providePresentationPhotoMapper(screenWidth: Int): PresentationPhotoMapper =
        PresentationPhotoMapper(screenWidth)

    @Provides
    @Singleton
    fun provideDataPhotoMapper(): DataPhotoMapper =
        DataPhotoMapper()
}