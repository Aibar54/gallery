package com.example.gallery.domain.model

data class Photo(
    val id: String,
    val width: Int,
    val height: Int,
    val srcUrl: String
)