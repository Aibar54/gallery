package com.example.gallery.domain.repository

import com.example.gallery.domain.model.Photo
import io.reactivex.rxjava3.core.Single

interface PhotoRepository {

    fun getPhotos(page: String): Single<List<Photo>>
}