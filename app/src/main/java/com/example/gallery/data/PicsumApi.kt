package com.example.gallery.data

import com.example.gallery.data.model.ImageDetail
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface PicsumApi {

    @GET("/v2/list")
    fun getImageDetails(
        @Query("page") page: String
    ): Single<List<ImageDetail>>

}