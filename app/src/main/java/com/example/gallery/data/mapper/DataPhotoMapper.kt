package com.example.gallery.data.mapper

import com.example.gallery.data.model.ImageDetail
import com.example.gallery.domain.model.Photo

class DataPhotoMapper {

    fun mapToDomain(image: ImageDetail): Photo =
        Photo(image.id, image.width, image.height, image.downloadUrl)

    fun mapToDomain(imageList: List<ImageDetail>) : List<Photo> =
        imageList.map { mapToDomain(it) }
}