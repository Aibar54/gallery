package com.example.gallery.data.model

import com.google.gson.annotations.SerializedName

data class ImageDetail(
    @SerializedName("id")
    val id: String,
    @SerializedName("author")
    val author: String,
    @SerializedName("width")
    val width: Int,
    @SerializedName("height")
    val height: Int,
    @SerializedName("download_url")
    val downloadUrl: String
)