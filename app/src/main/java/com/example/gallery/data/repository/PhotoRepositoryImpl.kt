package com.example.gallery.data.repository

import com.example.gallery.data.PicsumApi
import com.example.gallery.data.mapper.DataPhotoMapper
import com.example.gallery.domain.model.Photo
import com.example.gallery.domain.repository.PhotoRepository
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class PhotoRepositoryImpl @Inject constructor(
    private val api: PicsumApi,
    private val photoMapper: DataPhotoMapper
) : PhotoRepository {
    override fun getPhotos(page: String): Single<List<Photo>> {
        return api.getImageDetails(page)
            .map { photoMapper.mapToDomain(it) }
    }
}

